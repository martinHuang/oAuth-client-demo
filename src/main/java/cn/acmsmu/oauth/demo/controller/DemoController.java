package cn.acmsmu.oauth.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;

/**
 * @Desc:
 * @Author: huangzhiyuan
 * @CreateDate: 2023/2/14 23:37
 * @Modify:
 */
@RestController
public class DemoController {

    @GetMapping("/hello")
    public String hello() {
        String str = "Hello Word!";
        return JSON.toJSONString(str);
    }
}
