package cn.acmsmu.oauth.demo.controller;

import cn.acmsmu.oauth.demo.consts.OAuthConsts;
import cn.acmsmu.oauth.demo.facade.OAuthFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @Desc:
 * @Author: huangzhiyuan
 * @CreateDate: 2023/4/11 22:21
 * @Modify:
 */
@Controller
public class OAuthController {

    @Autowired
    private OAuthFacade oAuthFacade;

    @GetMapping("/")
    public String index(Model model) {
        model.addAttribute("clientId", OAuthConsts.CLIENT_ID);
        return "index";
    }

    @GetMapping("/callBack")
    @ResponseBody
    public String callBack(String code) {
        return oAuthFacade.getToken(code);
    }

    @GetMapping("/user")
    @ResponseBody
    public String getUserInfo() {
        return oAuthFacade.getUserInfo();
    }


}
