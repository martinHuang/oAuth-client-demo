package cn.acmsmu.oauth.demo.utils;

import com.alibaba.fastjson.JSON;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

/**
 * @Desc:
 * @Author: huangzhiyuan
 * @CreateDate: 2023/4/11 22:49
 * @Modify:
 */
public class NetUtil {

    private static RestTemplate restTemplate = new RestTemplate();

    public static Object doGet(String url, Map<String, Object> param, HttpHeaders headers, Class respObjClass) {
        HttpEntity<String> entity = new HttpEntity<>(JSON.toJSONString(param), headers);
        Object resp = restTemplate.exchange(url, HttpMethod.GET, entity, respObjClass).getBody();
        return resp;
    }

    public static Object doPost(String url, Map<String, Object> param, Class respObjClass) {
        Object resp = restTemplate.postForObject(url, param, respObjClass);
        return resp;
    }
}
