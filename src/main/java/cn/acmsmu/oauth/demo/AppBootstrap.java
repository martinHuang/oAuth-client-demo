package cn.acmsmu.oauth.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Desc:
 * @Author: huangzhiyuan
 * @CreateDate: 2023/2/14 23:27
 * @Modify:
 */
@SpringBootApplication
public class AppBootstrap {
    public static void main(String[] args) {
        SpringApplication.run(AppBootstrap.class, args);
    }
}
